
import time
import datetime

def filename2str(file_name):
    time_str = '2017-01-01 00:00:00.000'

    tt = time.strptime(time_str, "%Y-%m-%d %H:%M:%S.%f")
    tt = time.mktime(tt)
    tt = tt + (int(file_name[4:7]) - 1) * 24 * 60 * 60 + (8 + int(file_name[7:9])) * 3600 + int(
        file_name[9:11]) * 60 + int(file_name[11:13]) + int(file_name[13:16]) / 1000
    datetime_struct = datetime.datetime.fromtimestamp(tt)
    return datetime_struct.strftime('%Y%m%d%H%M%S%f')[:-3] + file_name[16:]

if __name__ == '__main__':
    file_name = '2017110034710150_XT2#__1_1.atr'
    print(filename2str(file_name))