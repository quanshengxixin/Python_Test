
import time
import multiprocessing

def print_hello(id):
    time.sleep(3)
    print("hello %d id"%id)

if __name__ == '__main__':
    pool = multiprocessing.Pool(processes=4)

    for i in range(19):
        pool.apply_async(print_hello, (i,))

    pool.close()
    pool.join()
    print("main end")